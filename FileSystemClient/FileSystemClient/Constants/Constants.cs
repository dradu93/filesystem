﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FileSystemClient.Constants
{
    public class Constants
    {
        public class Connection
        {
            public static readonly string IpAddress = "192.168.99.37";
            public static readonly int Port = 500;
        }

        public class ValidationRegex
        {
            public static readonly Regex Ipv4Address = 
                new Regex(@"^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$");

            public class Commands
            {
                public static readonly Regex Create =
                new Regex(@"^[\s]*creareResursa[\s]*([a-zA-Z\d]*)[\s]*([a-zA-Z\d]*)[\s]*([a-zA-Z\d\/\.-_]*)[\s]*(0|1)[\s]*(\'[\s\S]*\')?[\s]*$");

                public static readonly Regex Read =
                    new Regex(@"^[\s]*readResursa[\s]*([a-zA-Z\d]*)[\s]*([a-zA-Z\d]*)[\s]*([a-zA-Z\d\/\.-_]*)[\s]*$");

                public static readonly Regex Write =
                    new Regex(@"^[\s]*writeResursa[\s]*([a-zA-Z\d]*)[\s]*([a-zA-Z\d]*)[\s]*([a-zA-Z\d\/\.-]*)[\s]*(\'[\s\S]*\')[\s]*$");

                public static readonly Regex ChangeRights =
                    new Regex(@"^[\s]*changeRights[\s]*([a-zA-Z\d]*)[\s]*([a-zA-Z\d]*)[\s]*([a-zA-Z\d\/\.-]*)[\s]*(rw|r|w|'')[\s]*$");
            }

            public class Functions
            {
                public static readonly Regex Create = 
                    new Regex(@"^[\s]*creareResursa[\s]*\([\s]*([a-zA-Z\d]*)[\s]*,[\s]*([a-zA-Z\d]*)[\s]*,[\s]*([a-zA-Z\d\/\.-_]*)[\s]*,[\s]*(0|1)[\s]*(,[\s]*\'([\s\S]*)\')?\)[\s]*$");

                public static readonly Regex Read = 
                    new Regex(@"^[\s]*readResursa[\s]*\([\s]*([a-zA-Z\d]*)[\s]*,[\s]*([a-zA-Z\d]*)[\s]*,[\s*]([a-zA-Z\d\/\.-_]*)[\s]*\)[\s]*$");

                public static readonly Regex Write = 
                    new Regex(@"^[\s]*writeResursa[\s]*\([\s]*([a-zA-Z\d]*)[\s]*,[\s*]([a-zA-Z\d]*)[\s]*,[\s*]([a-zA-Z\d]*)[\s]*,[\s]*\'([\s\S]*)\'[\s]*\)[\s]*$");

                public static readonly Regex ChangeRights = 
                    new Regex(@"^[\s]*changeRights[\s]*\([\s]*([a-zA-Z\d]*)[\s]*,[\s]*([a-zA-Z\d]*)[\s]*,[\s*]([a-zA-Z\d\/\.-]*)[\s]*,[\s]*(rw|r|w|'')[\s]*\)[\s]*$");
            }

            
        }


    }
}
