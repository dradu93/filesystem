﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using FileSystemClient.Constants;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using FileSystemServer.Client;
using FileSystemServer.Constants;
using System.Reflection;

namespace FileSystemClient
{
    public partial class Form1 : Form
    {
        TcpClient TcpClient;

        List<string> CommandHistory;

        int CommandHistoryIndex;

        public Form1()
        {
            InitializeComponent();
            CommandHistory = new List<string>();
            CommandHistoryIndex = CommandHistory.Count;
        }

        private string GetLocalIPv4Address()
        {
            var networkInterface = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces()
                .FirstOrDefault(t => t.Description.Contains("Ethernet") 
                && t.Name.Contains("Local Area Connection"));
            
            var ip = networkInterface.GetIPProperties().UnicastAddresses
                .FirstOrDefault(t => 
                Constants.Constants.ValidationRegex.Ipv4Address.IsMatch(t.Address.ToString()));
            return ip.Address.ToString();
        }

        protected void Log(string info, bool putTimestamp = true)
        {
            
            try
            {
                string log_text;
                if (putTimestamp)
                {
                    log_text = string.Format("[{0}] {1}\r\n", DateTime.Now, info);
                }
                else
                {
                    log_text = string.Format("{0}\r\n", info);
                }
                
                textBox1.AppendText(log_text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    string.Format("The following exception was thrown while attempting to log info: {0}", 
                    ex.ToString()));
            }
        }

        private void ConnectEnableButton(bool state)
        {
            connectToolStripMenuItem.Enabled = state;
            testToolStripMenuItem.Enabled = !state;
            disconnectToolStripMenuItem.Enabled = !state;
            textBox2.Enabled = !state;
            if (!state)
            {
                toolStripStatusLabel4.Text = "Connected!";
            }
            else
            {
                toolStripStatusLabel4.Text = "Not connected!";
                
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = GetLocalIPv4Address();
            ConnectEnableButton(true);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripComboBox1_Click(object sender, EventArgs e)
        {

        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TcpClient = new TcpClient();
            TcpClient.Connect(Constants.Constants.Connection.IpAddress, Constants.Constants.Connection.Port);
            Log(string.Format("Connection to server successfully done({0}:{1})!", 
                Constants.Constants.Connection.IpAddress, Constants.Constants.Connection.Port));
            ConnectEnableButton(false);
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TcpClient.GetStream().Close();
            TcpClient.Close();
            Log(string.Format("Disconnection to server successfully done({0}:{1})!",
                Constants.Constants.Connection.IpAddress, Constants.Constants.Connection.Port));
            ConnectEnableButton(true);
        }

        private void SendCommand()
        {

            var formatter = new BinaryFormatter();
            ClientResponse response = null;
            var cmd = textBox2.Text;
            textBox2.Clear();
            textBox2.Enabled = false;
            CommandHistory.Add(cmd);
            CommandHistoryIndex = CommandHistory.Count;

            ClientRequest request = null;

            var validCmd = true;
            
            if (cmd.StartsWith("creareResursa"))//Constants.Constants.ValidationRegex.Commands.Create.IsMatch(cmd))
            {
                Log(string.Format("REQUEST -> {0}", cmd));
                MatchCollection matches;
                if (Constants.Constants.ValidationRegex.Commands.Create.IsMatch(cmd))
                {
                    matches = Regex.Matches(cmd, Constants.Constants.ValidationRegex.Commands.Create.ToString());
                }
                else if (Constants.Constants.ValidationRegex.Functions.Create.IsMatch(cmd))
                {
                    matches = Regex.Matches(cmd, Constants.Constants.ValidationRegex.Functions.Create.ToString());
                }
                else
                {
                    Log("ERROR: Request not in correct format!");
                    textBox2.Enabled = true;
                    textBox2.Select();
                    return;
                }
                
                var user = matches[0].Groups[1].Value;
                var password = matches[0].Groups[2].Value;
                var resourceName = matches[0].Groups[3].Value;
                var fileType = (FileType)int.Parse(matches[0].Groups[4].Value);

                var value = "";
                var valueGroup = matches[0].Groups[6];
                if ((fileType == FileType.File) && (valueGroup.Success))
                {
                    value = valueGroup.Value;
                }           

                request = new CreateRequest(user, password, resourceName, fileType, value);
                
                var byteArray = new byte[512];                
                using (var sendStream = new MemoryStream())
                {
                    formatter.Serialize(sendStream, request);
                    TcpClient.Client.Send(sendStream.ToArray());
                    TcpClient.Client.Receive(byteArray);
                    using (var receiveStream = new MemoryStream(byteArray))
                    {
                        response = (ClientResponse)formatter.Deserialize(receiveStream);
                    }
                }
            }
            else if (cmd.StartsWith("readResursa"))//Constants.Constants.ValidationRegex.Commands.Read.IsMatch(cmd))
            {
                Log(string.Format("REQUEST -> {0}", cmd));
                MatchCollection matches;
                if (Constants.Constants.ValidationRegex.Commands.Read.IsMatch(cmd))
                {
                    matches = Regex.Matches(cmd, Constants.Constants.ValidationRegex.Commands.Read.ToString());
                }
                else if (Constants.Constants.ValidationRegex.Functions.Read.IsMatch(cmd))
                {
                    matches = Regex.Matches(cmd, Constants.Constants.ValidationRegex.Functions.Read.ToString());
                }
                else
                {
                    Log("ERROR: Request not in correct format!");
                    textBox2.Enabled = true;
                    textBox2.Select();
                    return;
                }

                var user = matches[0].Groups[1].Value;
                var password = matches[0].Groups[2].Value;
                var resourceName = matches[0].Groups[3].Value;
                
                request = new ReadRequest(user, password, resourceName);

                var byteArray = new byte[512];
                using (var sendStream = new MemoryStream())
                {
                    formatter.Serialize(sendStream, request);
                    TcpClient.Client.Send(sendStream.ToArray());
                    TcpClient.Client.Receive(byteArray);
                    using (var receiveStream = new MemoryStream(byteArray))
                    {
                        response = (ClientResponse)formatter.Deserialize(receiveStream);
                    }
                }
            }
            else if (cmd.StartsWith("writeResursa"))//Constants.Constants.ValidationRegex.Commands.Write.IsMatch(cmd))
            {
                Log(string.Format("REQUEST -> {0}", cmd));
                MatchCollection matches;
                if (Constants.Constants.ValidationRegex.Commands.Write.IsMatch(cmd))
                {
                    matches = Regex.Matches(cmd, Constants.Constants.ValidationRegex.Commands.Write.ToString());
                }
                else if (Constants.Constants.ValidationRegex.Functions.Write.IsMatch(cmd))
                {
                    matches = Regex.Matches(cmd, Constants.Constants.ValidationRegex.Functions.Write.ToString());
                }
                else
                {
                    Log("ERROR: Request not in correct format!");
                    textBox2.Enabled = true;
                    textBox2.Select();
                    return;
                }
                
                
                var user = matches[0].Groups[1].Value;
                var password = matches[0].Groups[2].Value;
                var resourceName = matches[0].Groups[3].Value;
                var value = matches[0].Groups[4].Value;

                request = new WriteRequest(user, password, resourceName, value);

                var byteArray = new byte[512];
                using (var sendStream = new MemoryStream())
                {
                    formatter.Serialize(sendStream, request);
                    TcpClient.Client.Send(sendStream.ToArray());
                    TcpClient.Client.Receive(byteArray);
                    using (var receiveStream = new MemoryStream(byteArray))
                    {
                        response = (ClientResponse)formatter.Deserialize(receiveStream);
                    }
                }

            }
            else if (cmd.StartsWith("changeRights"))//Constants.Constants.ValidationRegex.Commands.ChangeRights.IsMatch(cmd))
            {
                Log(string.Format("REQUEST -> {0}", cmd));

                MatchCollection matches;
                if (Constants.Constants.ValidationRegex.Commands.ChangeRights.IsMatch(cmd))
                {
                    matches = Regex.Matches(cmd, Constants.Constants.ValidationRegex.Commands.ChangeRights.ToString());
                }
                else if (Constants.Constants.ValidationRegex.Functions.ChangeRights.IsMatch(cmd))
                {
                    matches = Regex.Matches(cmd, Constants.Constants.ValidationRegex.Functions.ChangeRights.ToString());
                }
                else
                {
                    Log("ERROR: Request not in correct format!");
                    textBox2.Enabled = true;
                    textBox2.Select();
                    return;
                }
                                
                var user = matches[0].Groups[1].Value;
                var password = matches[0].Groups[2].Value;
                var resourceName = matches[0].Groups[3].Value;
                var rights = matches[0].Groups[4].Value;

                request = new ChangeRightsRequest(user, password, resourceName, rights);

                var byteArray = new byte[512];
                using (var sendStream = new MemoryStream())
                {
                    formatter.Serialize(sendStream, request);
                    TcpClient.Client.Send(sendStream.ToArray());
                    TcpClient.Client.Receive(byteArray);
                    using (var receiveStream = new MemoryStream(byteArray))
                    {
                        response = (ClientResponse)formatter.Deserialize(receiveStream);
                    }
                }
            }
            else
            {
                validCmd = false;
                Log(string.Format("Invalid command: {0}", cmd));
            }
            if (validCmd && (response != null))
            {
                Log(string.Format("RESPONSE -> {0}", Enum.GetName(typeof(ReturnCode), response.ReturnCode)));
                if((request.RequestType == RequestType.ReadResursa) && (response.ReturnCode == ReturnCode.Ok))
                {
                    Log(((ReadResponse)response).Payload, false);
                }
            }
            textBox2.Enabled = true;
            textBox2.Select();
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            /*
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                SendCommand();
            }
            else if(e.KeyChar == Convert.ToChar(Keys.Up))
            {
                if(CommandHistory.Count == 0)
                {
                    return;
                }
                --CommandHistoryIndex;
                textBox2.Text = CommandHistory[CommandHistoryIndex];
            }
            else if(e.KeyChar == Convert.ToChar(Keys.Down))
            {

            }
            */
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendCommand();
            }
            else if (e.KeyCode == Keys.Up)
            {
                if ((CommandHistory.Count == 0) || (CommandHistoryIndex == 0))
                {
                    return;
                }
                
                --CommandHistoryIndex;
                textBox2.Text = CommandHistory[CommandHistoryIndex];
            }
            else if (e.KeyCode == Keys.Down)
            {
                if((CommandHistory.Count == 0) || (CommandHistory.Count == CommandHistoryIndex))
                {
                    return;
                }
                
                if(++CommandHistoryIndex < CommandHistory.Count)
                {
                    textBox2.Text = CommandHistory[CommandHistoryIndex];
                }
                else
                {
                    textBox2.Clear();
                }
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1.ActiveForm.Close();
        }

        private void allCasesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox2.Enabled = false;
            var testCommandsList = new List<string>();
            //string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Tests\All_Cases.txt");
            //string[] files = File.ReadAllLines(path);
            using (var file = new System.IO.StreamReader(@"../../Tests/All_Cases.txt"))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    if (!string.IsNullOrWhiteSpace(line))
                    {
                        testCommandsList.Add(line);
                    }
                }
            }

            for(var i = 0; i < testCommandsList.Count; ++i)
            {
                textBox2.Text = testCommandsList[i];
                SendCommand();
                textBox2.Clear();
            }


            textBox2.Enabled = true;
        }
    }
}
