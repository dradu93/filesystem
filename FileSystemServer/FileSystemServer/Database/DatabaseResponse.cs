﻿using FileSystemServer.Client;
using FileSystemServer.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.Database
{
    public class DatabaseResponse
    {
        public ClientResponse ClientResponse { get; set; }

        public uint ThreadIndex { get; set; }
        
        public DatabaseResponse(ClientResponse clientResponse, uint threadIndex)
        {
            ClientResponse = clientResponse;
            ThreadIndex = threadIndex;
        }
    }
}
