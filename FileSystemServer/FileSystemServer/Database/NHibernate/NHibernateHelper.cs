﻿using FileSystemServer.Models;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.Database.NHibernate
{
    /// <summary>
    /// Clasa cu metode utile pentru diverse operatii cu baza de date.
    /// </summary>
    public sealed class NHibernateHelper
    {
        public static void CreateDatabase()
        {
            try
            {
                var config = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012.ConnectionString(Constants.Constants.Database.ConnectionString).ShowSql())
                .Mappings(m => m.FluentMappings
                .AddFromAssemblyOf<FileType>()
                .AddFromAssemblyOf<User>()
                .AddFromAssemblyOf<File>()
                /*.AddFromAssemblyOf<EmagCategoryMap>()*/)
                .BuildConfiguration();

                var exporter = new SchemaExport(config);
                // drop the current database tables
                exporter.Execute(true, true, true);
                // readd them
                exporter.Execute(true, true, false);
                PopulateDatabase();
            }
            catch (Exception ex)
            {

            }
        }
        
        private static void PopulateDatabase()
        {
            try
            {
                using (var session = SessionFactory.OpenSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        // populate the FileType table
                        var names = Enum.GetNames(typeof(Constants.FileType));
                        var values = Enum.GetValues(typeof(Constants.FileType));
                        for (var i = 0; i < names.Length; ++i)
                        {
                            var ft = new FileType(names[i], (int)values.GetValue(i));
                            session.Save(ft);
                        }
                        tx.Commit();
                        
                    }

                    using (var tx = session.BeginTransaction())
                    {
                        var admin = new User("admin", "admin", true);
                        session.Save(admin);
                        var bob = new User("bob", "bob");
                        session.Save(bob);
                        var alice = new User("alice", "alice");
                        session.Save(alice);

                        var admin_folder = new File(admin, true);
                        session.Save(admin_folder);
                        var bob_folder = new File(bob, true, admin_folder);
                        session.Save(bob_folder);
                        var alice_folder = new File(alice, true, admin_folder);
                        session.Save(alice_folder);
                        tx.Commit();
                    }
                }               
            }
            catch(Exception ex)
            {

            }
        }

        public static ISessionFactory BuildSessionFactory()
        {
            var sessionFactory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012.ConnectionString(Constants.Constants.Database.ConnectionString).ShowSql())
                .Mappings(m => m.FluentMappings
                .AddFromAssemblyOf<FileType>()
                .AddFromAssemblyOf<User>()
                .AddFromAssemblyOf<File>()
                /*.AddFromAssemblyOf<EmagCategoryMap>()*/)
                .ExposeConfiguration(cfg => new SchemaExport(cfg).Create(false, false))
                .BuildSessionFactory();

            return sessionFactory;
        }
        
        private static readonly ISessionFactory _sessionFactory = BuildSessionFactory();
        public static ISessionFactory SessionFactory
        {
            get { return _sessionFactory; }
        }

        /// <summary>
        /// Opens a new NHibernate session.
        /// </summary>
        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }

        //public static ApplicationUser LoggerUser { get; set; }


        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
