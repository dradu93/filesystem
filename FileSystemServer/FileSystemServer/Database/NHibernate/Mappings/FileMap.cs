﻿using FileSystemServer.Models;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.NHibernate.Mappings
{
    public class FileMap : ClassMap<File>
    {
        public FileMap()
        {
            Id(t => t.Id).GeneratedBy.GuidComb();
            Map(t => t.Name).Unique().Not.Nullable();
            Map(t => t.Value).Not.Nullable();
            Map(t => t.R).Not.Nullable();
            Map(t => t.W).Not.Nullable();
            References(t => t.Type).Not.Nullable();
            References(t => t.Owner).Not.Nullable();
            References(t => t.Parent).Nullable();
        }
    }
}
