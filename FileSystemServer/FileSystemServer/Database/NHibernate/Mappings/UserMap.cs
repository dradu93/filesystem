﻿using FileSystemServer.Models;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.NHibernate.Mappings
{
    public class UserMap : ClassMap <User>
    {
        public UserMap()
        {
            Id(t => t.Id).GeneratedBy.GuidComb();
            Map(t => t.Name).Unique().Not.Nullable();
            Map(t => t.Password).Not.Nullable();
            Map(t => t.PersonalFolder).Not.Nullable();
            Map(t => t.SystemAdmin).Not.Nullable();
        }
    }
}
