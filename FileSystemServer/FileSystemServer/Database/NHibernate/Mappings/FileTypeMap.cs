﻿using FileSystemServer.Models;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.NHibernate.Mappings
{
    public class FileTypeMap : ClassMap<FileType>
    {
        public FileTypeMap()
        {
            Id(t => t.Id).GeneratedBy.GuidComb();
            Map(t => t.Name).Unique().Not.Nullable();
            Map(t => t.Value).Unique().Not.Nullable();
        }
    }
}
