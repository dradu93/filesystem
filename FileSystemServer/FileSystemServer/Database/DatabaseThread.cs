﻿using FileSystemServer.Client;
using FileSystemServer.Database.NHibernate;
using FileSystemServer.Models;
using FileSystemServer.Threading;
using NHibernate.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSystemServer.Database
{
    public class DatabaseThread : AThread
    {
        public ConcurrentQueue<DatabaseRequest> RequestQueue;

        //private Thread _thread;
        
        public DatabaseThread() : base()
        {            
            RequestQueue = new ConcurrentQueue<DatabaseRequest>();            
        }

        public override void Run()
        {
            while (true)
            {
                if (!RequestQueue.IsEmpty)
                {
                    DatabaseRequest req;
                    var result = RequestQueue.TryDequeue(out req);

                    if (result)
                    {
                        var databaseResponse = ProcessRequest(req);
                        var clientThread = Program.ClientThreadList.FirstOrDefault(t => t.ClientThreadIndex.Equals(req.ThreadIndex));
                        clientThread.ResponseQueue.Enqueue(databaseResponse.ClientResponse);
                    }                                      
                }
                Thread.Sleep(1000);
            }
        }

        private string StripFileName(string fullFileName)
        {
            var split = fullFileName.Split('/');
            return split[split.Length - 1];
        }

        private DatabaseResponse ProcessRequest(DatabaseRequest databaseRequest)
        {
            var clientRequest = databaseRequest.ClientRequest;

            // take the claimed user and password
            var claimedUser = clientRequest.User;
            var password = clientRequest.Password;

            // start working with the database
            using (var session = NHibernateHelper.SessionFactory.OpenSession())
            using (var tx = session.BeginTransaction())
            {
                // check authentification
                var user = session.Query<User>()
                    .FirstOrDefault(t => t.Name.Equals(claimedUser));
                if((user == null) || !password.Equals(user.Password))
                {
                    // user-pass combination is incorrect
                    var clientResponse = new ClientResponse(clientRequest.Index);
                    clientResponse.ReturnCode = Constants.ReturnCode.IncorrectUserPasswordCombination;
                    var databaseResponse = new DatabaseResponse(clientResponse, databaseRequest.ThreadIndex);
                    return databaseResponse;
                }
                // if the program reaches here, the authentification has been done succesfully!
                else if (clientRequest.GetType() == typeof(ReadRequest))
                {
                    var clientResponse = new ReadResponse(clientRequest.Index);
                    var targetedFile = session.Query<File>()
                        .FirstOrDefault(t => t.Name.Equals(clientRequest.ResourceName));

                    bool readAccess = (user.Equals(targetedFile.Owner) || targetedFile.R);
                    
                    if (!readAccess)
                    {
                        clientResponse.ReturnCode = Constants.ReturnCode.NotAuthorized;                        
                    }
                    else if(targetedFile.Type.Value == (int)Constants.FileType.File)
                    {
                        clientResponse.ReturnCode = Constants.ReturnCode.Ok;                        
                        clientResponse.Payload = targetedFile.Value;
                    }
                    else if(targetedFile.Type.Value == (int)Constants.FileType.Folder)
                    {
                        clientResponse.ReturnCode = Constants.ReturnCode.Ok;
                        var childrenFiles = session.Query<File>()
                            .Where(t => t.Parent.Name.Equals(targetedFile.Name));
                            //.Where(t => t.Name.Equals(targetedFile.Name) && t.Parent.Name.Equals(targetedFile.Parent.Name));
                        clientResponse.Payload = "";
                        if ((childrenFiles != null) && (childrenFiles.Count() > 0))
                        {
                            var list = childrenFiles.ToList();
                            for (var i = 0; i < list.Count; ++i)
                            {
                                clientResponse.Payload += StripFileName(list[i].Name) + "\r\n";
                            }
                        }
                    }
                    var databaseResponse = new DatabaseResponse(clientResponse, databaseRequest.ThreadIndex);
                    return databaseResponse;
                }
                else if (clientRequest.GetType() == typeof(WriteRequest))
                {
                    var clientResponse = new ReadResponse(clientRequest.Index);
                    var targetedFile = session.Query<File>()
                        .FirstOrDefault(t => t.Name.Equals(clientRequest.ResourceName));
                    
                    if(targetedFile == null)
                    {
                        clientResponse.ReturnCode = Constants.ReturnCode.NotExisting;
                    }
                    else
                    {
                        bool writeAccess = (user.Equals(targetedFile.Owner) || targetedFile.W);
                        if (!writeAccess)
                        {
                            clientResponse.ReturnCode = Constants.ReturnCode.NotAuthorized;
                        }
                        else if (targetedFile.Type.Value == (int)Constants.FileType.File)
                        {
                            targetedFile.Value = ((WriteRequest)clientRequest).Value;
                            session.Update(targetedFile);
                            clientResponse.ReturnCode = Constants.ReturnCode.Ok;
                        }
                        else if (targetedFile.Type.Value == (int)Constants.FileType.Folder)
                        {

                        }
                    }
                    var databaseResponse = new DatabaseResponse(clientResponse, databaseRequest.ThreadIndex);
                    return databaseResponse;
                }
                else if (clientRequest.GetType() == typeof(CreateRequest))
                {
                    var clientResponse = new CreateResponse(clientRequest.Index);
                    var file = session.Query<File>()
                        .FirstOrDefault(t => t.Name.Equals(clientRequest.ResourceName));                    
                    
                    if (file == null)
                    {
                        var parentFileName = ClientRequest.GetParentFolder(clientRequest.ResourceName);
                        var parentFile = session.Query<File>()
                            .FirstOrDefault(t => t.Name.Equals(parentFileName));
                        if (!parentFile.Owner.Equals(user) && !parentFile.W)
                        {
                            clientResponse.ReturnCode = Constants.ReturnCode.NotAuthorized;
                        }
                        else
                        {
                            var createRequest = (CreateRequest)clientRequest;
                            var fileType = session.Query<FileType>()
                                .FirstOrDefault(t => t.Value.Equals((int)createRequest.FileType));
                            var newFile = new File(clientRequest.ResourceName, fileType, user, false, parentFile, createRequest.Value);
                            session.Save(newFile);
                            newFile.R = parentFile.R;
                            newFile.W = parentFile.W;
                            clientResponse.ReturnCode = Constants.ReturnCode.Ok;
                        }
                    }
                    else
                    {
                        clientResponse.ReturnCode = Constants.ReturnCode.AlreadyExisting;
                    }
                    tx.Commit();
                    return new DatabaseResponse(clientResponse, databaseRequest.ThreadIndex);
                }
                else if (clientRequest.GetType() == typeof(ChangeRightsRequest))
                {
                    var clientResponse = new ReadResponse(clientRequest.Index);
                    var targetedFile = session.Query<File>()
                        .FirstOrDefault(t => t.Name.Equals(clientRequest.ResourceName));
                    if(targetedFile == null)
                    {
                        clientResponse.ReturnCode = Constants.ReturnCode.NotExisting;
                    }
                    else if(!(targetedFile.Owner.Equals(user) || user.SystemAdmin))
                    {
                        clientResponse.ReturnCode = Constants.ReturnCode.NotAuthorized;
                    }
                    else
                    {
                        var requestedRights = ((ChangeRightsRequest)clientRequest).Rights.ToLower();
                        targetedFile.R = requestedRights.Contains('r');
                        targetedFile.W = requestedRights.Contains('w');
                        session.Update(targetedFile);
                        clientResponse.ReturnCode = Constants.ReturnCode.Ok;
                    }
                    tx.Commit();
                    return new DatabaseResponse(clientResponse, databaseRequest.ThreadIndex);
                }
                tx.Commit();
            }
            

            return null;
        }
    }
}
