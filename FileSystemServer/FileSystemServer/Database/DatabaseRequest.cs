﻿using FileSystemServer.Client;
using FileSystemServer.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.Database
{
    public class DatabaseRequest
    {
        public ClientRequest ClientRequest { get; set; }

        public uint ThreadIndex { get; set; }

        public DatabaseRequest(ClientRequest clientRequest, uint threadIndex)
        {
            ClientRequest = clientRequest;
            ThreadIndex = threadIndex;
        }
    }
}
