﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.Models
{
    public class FileType
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }

        public virtual int Value { get; set; }

        public FileType(string name, int value)
        {
            Name = name;
            Value = value;
        }

        public FileType()
        {

        }
    }
}
