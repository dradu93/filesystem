﻿using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileSystemServer.Constants;

namespace FileSystemServer.Models
{
    public class File : IEquatable<File>
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Value { get; set; }
        public virtual FileType Type { get; set; }
        public virtual User Owner { get; set; }
        public virtual bool R { get; set; }
        public virtual bool W { get; set; }
        public virtual File Parent { get; set; }

        public File()
        {

        }

        public File(User owner, bool personalFolder, File parent = null)
        {
            Owner = owner;
            Parent = parent;
            using (var session = Database.NHibernate.NHibernateHelper.OpenSession())
            using (var tx = session.BeginTransaction())
            {
                if (personalFolder)
                {
                    var folderType = session.Query<FileType>()
                        .FirstOrDefault(t => t.Value.Equals((int)(Constants.FileType.Folder)));

                    Type = folderType;
                    Name = owner.PersonalFolder;
                    Value = "";
                    R = false;
                    W = false;
                }
                else
                {
                    //var fileType
                }
                tx.Commit();
            }
                
        }

        public File(string fileName, FileType fileType, User owner, bool personalFolder, File parent = null, string fileContent = "")
            : this(owner, personalFolder, parent)
        {
            Name = fileName;
            Value = fileContent;
            Type = fileType;
        }

        public virtual bool Equals(File other)
        {

            return ((other.Parent != null) && (Name.Equals(other.Name)) && (Parent.Equals(other.Parent)));
        }
    }
}
