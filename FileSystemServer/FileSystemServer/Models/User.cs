﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.Models
{
    public class User : IEquatable<User>
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Password { get; set; }
        public virtual string PersonalFolder { get; set; }
        public virtual bool SystemAdmin { get; set; }

        public User()
        {

        }

        public User(string name, string password, bool systemAdmin = false)
        {
            Name = name;
            Password = password;
            PersonalFolder = "/";
            if (!systemAdmin)
            {
                PersonalFolder += name;
            }            
            SystemAdmin = systemAdmin;
        }

        public User(string name, string password, string personalFolder, bool systemAdmin = false) 
            : this(name, password, systemAdmin)
        {
            PersonalFolder = personalFolder;
        }

        public virtual bool Equals(User other)
        {
            return this.Name.Equals(other.Name);
        }
    }
}
