﻿using FileSystemServer.Client;
using FileSystemServer.Database.NHibernate;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSystemServer
{
    public partial class Form1 : Form
    {        
        public Form1()
        {
            InitializeComponent();
        }

        private string GetLocalIPv4Address()
        {
            var networkInterface = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces()
                .FirstOrDefault(t => t.Description.Contains("Ethernet") && t.Name.Contains("Local Area Connection"));


            var regex = new Regex(@"^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$");

            var ip = networkInterface.GetIPProperties().UnicastAddresses
                .FirstOrDefault(t => regex.IsMatch(t.Address.ToString()));
            return ip.Address.ToString();
        }

        protected void Log(string info)
        {
            try
            {
                var log_text = string.Format("[{0}] {1}\r\n", DateTime.Now, info);
                textBox1.AppendText(log_text);
            }
            catch(Exception ex)
            {

            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Log("Loading Form1....");
            if (NHibernateHelper.SessionFactory != null)
            {
                toolStripStatusLabel4.Text = "Connected!";
                Log("Connection to database successful!");
            }
            Log(string.Format("Waiting for connections on port {0}...", Constants.Constants.Connection.Port));
            toolStripStatusLabel2.Text = GetLocalIPv4Address();
        }

        /*
        private void MessageQueue_Changed(object sender, EventArgs e)
        {
            while(MessageQueue.Count > 0)
            {
                var message = MessageQueue.Dequeue();
                Log(message);
            }
        }
        */
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripStatusLabel3_Click(object sender, EventArgs e)
        {

        }

        private void reCreateDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NHibernateHelper.CreateDatabase();
            Log("Database regenerated!");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Application.Exit();
        }
    }
}
