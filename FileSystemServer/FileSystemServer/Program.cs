﻿using FileSystemServer.Client;
using FileSystemServer.ConnectionHandler;
using FileSystemServer.Database;
using FileSystemServer.Database.NHibernate;
using FileSystemServer.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSystemServer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        //[STAThread]
        public static UIThread UIThread;
        public static DatabaseThread DBThread;
        public static ConnectionHandlerThread CHThread;
        public static List<ClientThread> ClientThreadList;
        public static object LockObj;

        static void Main()
        {
            LockObj = new object();
            // initialize the UI thread
            UIThread = new UIThread();
            UIThread.Start();

            // initialize the database thread
            DBThread = new DatabaseThread();
            DBThread.Start();
            MessageBox.Show("ok!");

            // initialize the client thread pool
            ClientThreadList = new List<ClientThread>();

            // start the connection handler port
            CHThread = new ConnectionHandlerThread();
            CHThread.Start();
        }
    }
}
