﻿using FileSystemServer.Database;
using FileSystemServer.Models;
using FileSystemServer.Threading;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSystemServer.Client
{
    public class ClientThread : AThread
    {
        private Socket _socket { get; set; }

        public Queue<ClientRequest> RequestQueue;
        public Queue<ClientResponse> ResponseQueue;

        public uint ClientThreadIndex { get; set; }

        private byte[] _receiveBuffer { get; set; }
        
        public ClientThread(Socket socket, uint index) : base()
        {
            _socket = socket;
            _socket.ReceiveTimeout = 1000;
            this.ClientThreadIndex = index;
            RequestQueue = new Queue<ClientRequest>();
            ResponseQueue = new Queue<ClientResponse>();
            _receiveBuffer = new byte[256];
        }

        public override void Run()
        {
            var formatter = new BinaryFormatter();
            while (true)
            {
                if (_socket.Poll(1, SelectMode.SelectRead) && (_socket.Available == 0))
                {
                    break;
                }
                else 
                {
                    try
                    {
                        var byteArray = new byte[512];
                        var x = _socket.Receive(byteArray);
                        var text = System.Text.Encoding.UTF8.GetString(byteArray);
                        using (var ms = new MemoryStream(byteArray))
                        {
                            var req = (ClientRequest)formatter.Deserialize(ms);
                            RequestQueue.Enqueue(req);
                        }
                    }
                    catch(Exception ex)
                    {
                        /*
                        Program.UIThread.UIForm.MessageQueue.Enqueue(
                            string.Format("The following exception was thrown trying to receive a command: {0}", ex.ToString()));
                        */
                    }
                    
                    if (RequestQueue.Count > 0)
                    {
                        // send request to database
                        Program.DBThread.RequestQueue.Enqueue(new DatabaseRequest(RequestQueue.Dequeue(), ClientThreadIndex));
                    }
                    if(ResponseQueue.Count > 0)
                    {
                        using (var sendStream = new MemoryStream())
                        {
                            formatter.Serialize(sendStream, ResponseQueue.Dequeue());
                            _socket.Send(sendStream.ToArray());
                        }
                    }
                }
                Thread.Sleep(1000);
            }
            
            lock (Program.LockObj)
            {
                Program.ClientThreadList.Remove(this);
            }
            Program.UIThread.UIForm.MessageQueue.Enqueue(string.Format("Client with ID={0} has been disconnected!", ClientThreadIndex));
            _socket.Close();
        }

    }
}
