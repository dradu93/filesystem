﻿using FileSystemServer.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.Client
{
    [Serializable]
    public class ClientResponse : ISerializable
    {
        public uint Index { get; set; }

        public ReturnCode ReturnCode { get; set; }

        public ClientResponse(uint index)
        {
            Index = index;
        }

        public ClientResponse(uint index, ReturnCode returnCode) 
            : this(index)
        {
            ReturnCode = returnCode;
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("index", Index, typeof(uint));
            info.AddValue("returnCode", ReturnCode, typeof(ReturnCode));
        }

        public ClientResponse(SerializationInfo info, StreamingContext context)
        {
            Index = (uint)info.GetValue("index", typeof(uint));
            ReturnCode = (ReturnCode)info.GetValue("returnCode", typeof(ReturnCode));
        }
    }

    [Serializable]
    public class ReadResponse : ClientResponse
    {
        public string Payload { get; set; }

        public ReadResponse(uint index) 
            : base(index)
        {

        }
        
        public ReadResponse(uint index, ReturnCode returnCode, string payload)
            : base(index, returnCode)
        {
            Payload = payload;
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("payload", Payload, typeof(string));
        }

        public ReadResponse(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Payload = (string)info.GetValue("payload", typeof(string));
        }
    }

    [Serializable]
    public class WriteResponse : ClientResponse
    {
        public WriteResponse(uint index)
            : base(index)
        {

        }

        public WriteResponse(uint index, ReturnCode returnCode)
            : base(index, returnCode)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public WriteResponse(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }
    }

    [Serializable]
    public class CreateResponse : ClientResponse
    {
        public CreateResponse(uint index)
            : base(index)
        {

        }

        public CreateResponse(uint index, ReturnCode returnCode)
            : base(index, returnCode)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public CreateResponse(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }
    }

    [Serializable]
    public class ChangeRightsResponse: ClientResponse
    {
        public ChangeRightsResponse(uint index)
            : base(index)
        {

        }

        public ChangeRightsResponse(uint index, ReturnCode returnCode)
            : base(index, returnCode)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public ChangeRightsResponse(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }
    }


}
