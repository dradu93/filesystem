﻿using FileSystemServer.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FileSystemServer.Client
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public abstract class ClientRequest : ISerializable
    {
        public uint Index { get; set; }
        public RequestType RequestType { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string ResourceName { get; set; }

        public ClientRequest(string user, string password, string resourceName)
        {
            User = user;
            Password = password;
            ResourceName = resourceName;
        }

        // Method implemented for ISerializable interface
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("index", Index, typeof(uint));
            info.AddValue("requestType", RequestType, typeof(RequestType));
            info.AddValue("user", User, typeof(string));
            info.AddValue("password", Password, typeof(string));
            info.AddValue("resourceName", ResourceName, typeof(string));
        }

        public ClientRequest(SerializationInfo info, StreamingContext context)
        {
            Index = (uint)info.GetValue("index", typeof(uint));
            RequestType = (RequestType)info.GetValue("requestType", typeof(RequestType));
            User = (string)info.GetValue("user", typeof(string));
            Password = (string)info.GetValue("password", typeof(string));
            ResourceName = (string)info.GetValue("resourceName", typeof(string));
        }

        public static string GetParentFolder(string fileName)
        {
            if(fileName == "/")
            {
                return "/";
            }
            var pattern = @"\/[\w\-. ]+";
            //var regex = new Regex();
            var parentFolder = "";
            var matches = Regex.Matches(fileName, pattern);
            var n = matches.Count;
            /*
            if(n == 1)
            {
                return "/";
            }
            */
            for(var i = 0; i < n - 1; ++i)
            {
                parentFolder += matches[i].Value;
            }
            return parentFolder;
        }
    }

    [Serializable]
    public class CreateRequest : ClientRequest
    {
        public CreateRequest(string user, string password, string resourceName, FileType fileType, string value)
            : base(user, password, resourceName)
        {
            FileType = fileType;
            Value = value;
            RequestType = RequestType.CreareResursa;
        }

        public FileType FileType { get; set; }
        public string Value { get; set; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("fileType", FileType, typeof(FileType));
            info.AddValue("value", Value, typeof(string));
        }

        public CreateRequest(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            FileType = (FileType)info.GetValue("fileType", typeof(FileType));
            Value = (string)info.GetValue("value", typeof(string));
        }
    }

    [Serializable]
    public class ReadRequest : ClientRequest
    {
        public ReadRequest(string user, string password, string resourceName) 
            : base(user, password, resourceName)
        {
            RequestType = RequestType.ReadResursa;
        }
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public ReadRequest(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }
    }

    [Serializable]
    public class WriteRequest : ClientRequest
    {
        public WriteRequest(string user, string password, string resourceName, string value) 
            : base(user, password, resourceName)
        {
            Value = value;
            RequestType = RequestType.WriteResursa;
        }

        public string Value { get; set; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("value", Value, typeof(string));
        }

        public WriteRequest(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Value = (string)info.GetValue("value", typeof(string));
        }
    }

    [Serializable]
    public class ChangeRightsRequest : ClientRequest
    {
        public ChangeRightsRequest(string user, string password, string resourceName, string rights) 
            : base(user, password, resourceName)
        {
            Rights = rights;
            RequestType = RequestType.ChangeRights;
        }

        public string Rights { get; set; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("rights", Rights, typeof(string));
        }

        public ChangeRightsRequest(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Rights = (string)info.GetValue("rights", typeof(string));
        }
    }

}
