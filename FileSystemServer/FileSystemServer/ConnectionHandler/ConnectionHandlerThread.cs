﻿using FileSystemServer.Client;
using FileSystemServer.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.ConnectionHandler
{
    public class ConnectionHandlerThread : AThread
    {
        //public static uint ClientCounter = 0;

        public override void Run()
        {
            var ipAddress = IPAddress.Parse(Constants.Constants.Connection.IpAddress);
            var listener = new TcpListener(ipAddress, Constants.Constants.Connection.Port);
            listener.Start();

            while (true)
            {
                var clientSocket = listener.AcceptSocket();
                var id = (uint)Program.ClientThreadList.Count;
                var clientThread = new ClientThread(clientSocket, id);                
                Program.ClientThreadList.Add(clientThread);
                clientThread.Start();
                Program.UIThread.UIForm.MessageQueue.Enqueue(
                    string.Format("New client with ID={0} has been connected succesfully!", id));
            }
        }
    }
}
