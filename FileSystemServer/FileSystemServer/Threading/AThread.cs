﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileSystemServer.Threading
{
    public abstract class AThread
    {
        protected Thread _thread;

        public virtual void Start()
        {
            _thread.Start();
        }

        public void Join()
        {
            _thread.Join();
        }
        public bool IsAlive
        {
            get
            {
                return _thread.IsAlive;
            }
        }

        public AThread()
        {
            _thread = new Thread(new ThreadStart(this.Run));
        }

        public abstract void Run();
    }
}
