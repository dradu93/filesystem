﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.Constants
{
    /// <summary>
    /// Types of files
    /// </summary>
    public enum FileType
    {
        Folder = 0,
        File = 1
    }

    /// <summary>
    /// Types of requests received from the clients
    /// </summary>
    public enum RequestType
    {
        CreareResursa = 0,
        ReadResursa = 1,
        WriteResursa = 2,
        ChangeRights = 3
    }

    /// <summary>
    /// Return codes for client operations
    /// </summary>
    public enum ReturnCode
    {
        Ok = 0,
        NotAuthorized = 1,
        NotExisting = 2,
        AlreadyExisting = 3,
        IncorrectUserPasswordCombination = 4
    }
}
