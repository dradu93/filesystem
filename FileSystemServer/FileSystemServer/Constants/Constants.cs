﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.Constants
{
    public class Constants
    {
        public class Database
        {
            public static readonly string ConnectionString =
                @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\RDaia\Documents\FileSystem.mdf;Integrated Security=True;Connect Timeout=30";
        }

        public class Connection
        {
            public static readonly string IpAddress = "192.168.99.37";
            public static readonly int Port = 500;
        }
    }
}
