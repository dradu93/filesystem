﻿using FileSystemServer.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.UI
{
    public class EnhancedForm : Form1
    {
        public ExperimentalConcurrentQueue<string> MessageQueue;

        public EnhancedForm()
            : base()
        {
            MessageQueue = new ExperimentalConcurrentQueue<string>();
            MessageQueue.Changed += MessageQueue_Changed;
        }

        private void MessageQueue_Changed(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<object, EventArgs>(MessageQueue_Changed), sender, e);
                return;
            }

            while(MessageQueue.Count > 0)
            {
                var message = MessageQueue.Dequeue();
                Log(message);
            }

            /*
            // todo delegation
            while (MessageQueue.Count > 0)
            {
                var message = MessageQueue.Dequeue();

                Log(message);
            }
            */
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // EnhancedForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(553, 321);
            this.Name = "EnhancedForm";
            this.Load += new System.EventHandler(this.EnhancedForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void EnhancedForm_Load(object sender, EventArgs e)
        {

        }
    }
}
