﻿using FileSystemServer.Client;
using FileSystemServer.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemServer.UI
{
    public class UIThread : AThread
    {
        
        public EnhancedForm UIForm;

        public UIThread() : base()
        {            
            UIForm = new EnhancedForm();            
        }

        

        public override void Run()
        {
            UIForm.ShowDialog();
        }
    }
}
